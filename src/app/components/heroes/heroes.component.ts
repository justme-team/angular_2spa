import { Component, OnInit } from '@angular/core';
import { HeroesService, Heroe } from '../../services/heroes.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styles: []
})
export class HeroesComponent implements OnInit {

  heroes: Heroe[] = [];

  constructor( private _heroesService: HeroesService,
              private router: Router) {
    console.log('Contructor'); // Se ejecuta mucho anter
  }

  ngOnInit() {
    this.heroes = this._heroesService.getHeroes();
    console.log('ngOnInit'); // Se ejecuta después de que todo esté renderizado

    console.log(this.heroes);
  }

  verHeroe(id: string) {
    this.router.navigate(['/heroe', id]);
  }
}
