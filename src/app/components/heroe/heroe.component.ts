import { Component } from '@angular/core';
import { ActivatedRoute  } from '@angular/router';
import { HeroesService, Heroe } from '../../services/heroes.service';

@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html',
  styles: []
})
export class HeroeComponent {

  heroe: Heroe;

  constructor( private activatedRoute: ActivatedRoute,
                private heroesService: HeroesService) {
    this.activatedRoute.params.subscribe( params => {
      this.heroe = this.heroesService.getHeroe(params.id);
    });
  }

  public getCasaImage(heroe: Heroe): string {
    if (heroe.casa === 'Marvel') {
      return 'assets/img/marvel-logo.png';
    } else {
      return 'assets/img/dc-logo.png';
    }
  }

}
