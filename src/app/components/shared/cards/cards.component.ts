import { Component, Input , Output, EventEmitter} from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styles: []
})
export class CardsComponent  {

  @Input() hero: any;
  @Output() heroSelected: EventEmitter<number>;

  constructor( private router: Router) {
    this.heroSelected = new EventEmitter();
   }

  verHeroe() {
    this.router.navigate(['/heroe', this.hero.id]);
    // this.heroSelected.emit(this.hero.id);
  }

}
