import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HeroesService, Heroe } from '../../services/heroes.service';

@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.component.html'
})
export class BuscarComponent implements OnInit {

  heroes: Heroe[];
  id: string;
  emptySearch = true;

  constructor(  private activatedRoute: ActivatedRoute,
                private heroesService: HeroesService) {
    this.activatedRoute.params.subscribe( params => {
      this.id = params.id;
      this.heroes = heroesService.buscarHeroe(this.id);
      if (this.heroes.length > 0) {
        this.emptySearch = false;
      }
    });
  }

  ngOnInit() {
  }
}
